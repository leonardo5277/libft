/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmunoz <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/29 16:44:18 by lmunoz            #+#    #+#             */
/*   Updated: 2016/11/15 19:50:32 by lmunoz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(const char *str, unsigned int start, size_t len)
{
	char	*result;

	if (!str)
		return (NULL);
	result = ft_strnew(len);
	if (!result)
		return (NULL);
	str += start;
	return (ft_strncpy(result, str, len));
}
