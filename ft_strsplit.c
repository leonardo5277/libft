/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmunoz <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/29 17:18:53 by lmunoz            #+#    #+#             */
/*   Updated: 2016/10/20 17:36:40 by lmunoz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static char		**add_str(char **list, char *str)
{
	size_t	elem;
	char	**new_list;

	if (!str || !*str)
		return (list);
	elem = 0;
	while (list[elem])
		++elem;
	new_list = malloc(sizeof(char *) * (elem + 2));
	if (new_list == NULL)
		return (NULL);
	elem = 0;
	while (list[elem])
	{
		new_list[elem] = list[elem];
		++elem;
	}
	new_list[elem++] = str;
	new_list[elem] = NULL;
	free(list);
	return (new_list);
}

char			**ft_strsplit(char const *str, char c)
{
	char	**list;
	int		start;
	int		end;

	if (!str)
		return (NULL);
	list = malloc(sizeof(char *));
	if (list == NULL)
		return (NULL);
	list[0] = NULL;
	if (!*str)
		return (list);
	start = 0;
	end = 0;
	while (str[start])
	{
		while (str[start] == c)
			++start;
		end = start;
		while (str[end] != c && str[end] != '\0')
			++end;
		list = add_str(list, ft_strsub(str, start, end - start));
		start = end;
	}
	return (list);
}
