/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strcspn.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmunoz <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/25 23:07:04 by lmunoz            #+#    #+#             */
/*   Updated: 2016/06/29 20:40:10 by lmunoz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strcspn(const char *s1, const char *reject)
{
	size_t value;

	value = 0;
	while (*s1)
	{
		if (ft_strchr(reject, *s1))
			return (value);
		else
		{
			s1++;
			value++;
		}
	}
	return (value);
}
