/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmunoz <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/01/29 17:46:37 by lmunoz            #+#    #+#             */
/*   Updated: 2016/02/11 14:37:58 by lmunoz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *str, char (*f)(unsigned int, char))
{
	char	*cpy;
	int		pos;

	if (!str || !f)
		return (NULL);
	cpy = ft_strnew(ft_strlen(str));
	if (!cpy)
		return (NULL);
	pos = -1;
	while (str[++pos])
		cpy[pos] = f(pos, str[pos]);
	cpy[pos] = '\0';
	return (cpy);
}
