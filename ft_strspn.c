/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strspn.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lmunoz <marvin@42.fr>                      +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/06/25 23:22:40 by lmunoz            #+#    #+#             */
/*   Updated: 2016/06/29 20:39:25 by lmunoz           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

size_t		ft_strspn(const char *s1, const char *accept)
{
	size_t		value;

	value = 0;
	while (*s1 && ft_strchr(accept, *s1++))
		value++;
	return (value);
}
